<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');
//require_once('hewan.php');

$sheep = new animal("Shaun");

echo "Nama : $sheep->name <br> ";// "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br>"; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama : $kodok->name <br> ";
echo "Legs : $kodok->legs <br>"; 
echo "cold blooded : $kodok->cold_blooded <br>";
echo "Jump : ";
$kodok->jump() ; // "hop hop"
echo "<br>";
echo "<br>";
$sungokong = new ape ("Kera Sakti");
echo "Nama : $sungokong->name <br> ";
echo "Legs : $sungokong->legs <br>"; 
echo "cold blooded : $sungokong->cold_blooded <br>";
echo "Yell : ";
echo  $sungokong->yell(); // "Auooo"

?>